<?php

namespace Scandiweb\Source;

use Scandiweb\Database\QueryBuilder;

abstract class Product
{

    protected $title;
    protected $price;
    protected $q;

    public function __construct(string $title, int $price, QueryBuilder $q)
    {
        $this->title = $title;
        $this->price = $price;
        $this->q = $q;
    }


    public static function getTable()
    {
        $className = get_called_class();
        $table = substr(strtolower($className), strrpos($className, '\\') + 1);

        if ($table !== 'furniture') {
            return $table . 's';
        }

        return $table;
    }


    public function getAttribute(int $id, string $attr)
    {
        return $this->q->selectAttribute($id, $attr, self::getTable());
    }

    public function getProductName(int $id)
    {
        $results = $this->q->select($id, self::getTable());

        return $results[0]->title;
    }
}
